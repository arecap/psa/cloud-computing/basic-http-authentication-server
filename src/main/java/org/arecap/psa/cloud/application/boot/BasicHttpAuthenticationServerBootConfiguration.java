package org.arecap.psa.cloud.application.boot;


import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.ProviderSettings;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Configuration
@Import(OAuth2AuthorizationServerConfiguration.class)
public class BasicHttpAuthenticationServerBootConfiguration {

    @Value("${custom.authorizationserver.issuer-uri:http://127.0.0.1:8090}")
    private String issuerUri;

    @Autowired
    private OAuth2CustomAuthenticationServerClientRegistryConfiguration oAuth2CustomAuthenticationServerClientRegistryConfiguration;

    @Bean
    public RegisteredClientRepository registeredClientRepository() {
        return new InMemoryRegisteredClientRepository(autowiredClientConfiguration());
    }

    private List<RegisteredClient> autowiredClientConfiguration() {
        if(oAuth2CustomAuthenticationServerClientRegistryConfiguration.getClients() == null ||
                oAuth2CustomAuthenticationServerClientRegistryConfiguration.getClients().isEmpty()) {
            return getDefaultClient();
        }
        return oAuth2CustomAuthenticationServerClientRegistryConfiguration.getClients()
                .stream().map(this::registerClient).collect(Collectors.toList());
    }

    private RegisteredClient registerClient(OAuth2CustomAuthenticationServerClientRegistryConfiguration.OAuth2AuthorizationClientConfiguration authorizationServerClientConfiguration) {
        return RegisteredClient.withId(UUID.randomUUID().toString())
                .clientId(authorizationServerClientConfiguration.getClientId())
                .clientSecret(authorizationServerClientConfiguration.getClientSecret())
                .clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .redirectUri(authorizationServerClientConfiguration.getRedirectUri())
                .scope(authorizationServerClientConfiguration.getScope())
                .build();
    }

    private List<RegisteredClient> getDefaultClient() {
        return Arrays.asList(RegisteredClient.withId(UUID.randomUUID().toString())
                .clientId("atlas-client")
                .clientSecret("secret")
                .clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .redirectUri("http://localhost:8080/login/oauth2/code/articles-client-oidc")
                .redirectUri("http://localhost:8080/authorized")
                .scope(OidcScopes.OPENID)
                .scope("access.read")
                .build());
    }

    @Bean
    public JWKSource<SecurityContext> jwkSource() {
        RSAKey rsaKey = generateRsa();
        JWKSet jwkSet = new JWKSet(rsaKey);
        return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
    }

    private static RSAKey generateRsa() {
        KeyPair keyPair = generateRsaKey();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        return new RSAKey.Builder(publicKey)
                .privateKey(privateKey)
                .keyID(UUID.randomUUID().toString())
                .build();
    }

    private static KeyPair generateRsaKey() {
        KeyPair keyPair;
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            keyPair = keyPairGenerator.generateKeyPair();
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
        return keyPair;
    }

    @Bean
    public ProviderSettings providerSettings() {
        return new ProviderSettings().issuer(issuerUri);
    }


}
