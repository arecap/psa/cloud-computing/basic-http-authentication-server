package org.arecap.psa.cloud.application.boot;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "custom.authorizationserver")
public class OAuth2CustomAuthenticationServerClientRegistryConfiguration {

    private List<OAuth2AuthorizationClientConfiguration> clients;

    public List<OAuth2AuthorizationClientConfiguration> getClients() {
        return clients;
    }

    public void setClients(List<OAuth2AuthorizationClientConfiguration> clients) {
        this.clients = clients;
    }

    public static class OAuth2AuthorizationClientConfiguration {
        private String clientId;
        private String clientSecret;
        private String redirectUri;
        private String scope;

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }

        public String getRedirectUri() {
            return redirectUri;
        }

        public void setRedirectUri(String redirectUri) {
            this.redirectUri = redirectUri;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }
    }
}
