package org.arecap.psa.cloud.application;

import org.arecap.psa.cloud.boot.support.YamlPropertySourceFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ConfigurationPropertiesScan
@PropertySource(value = "classpath:oauth2clients.yaml", factory = YamlPropertySourceFactory.class)
public class BasicHttpAuthenticationServer {

    public static void main(String[] args) {
        SpringApplication.run(BasicHttpAuthenticationServer.class, args);
    }

}
