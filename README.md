# Basic Http Authentication Server
Implements a form login default spring security oauth2 authentication server 
This implementation follows https://www.baeldung.com/spring-security-oauth-auth-server
This example use Configuration Properties as in this guides https://www.baeldung.com/configuration-properties-in-spring-boot
https://www.baeldung.com/spring-boot-yaml-list

This can be use in case your organization use custom Database User Management with custom Password Encoder to migrate to cloud compliant computing services integration
This demo can be use in extension to integrate any Web Tier Application into a cloud services Identity Provider Consumer SSO.
